#!/usr/bin/python

from ming import *
from univers import __version__
from os.path import abspath, dirname

Ming_setScale(20)
Ming_useSWFVersion(7)

def make():
    movie = SWFMovie()
    movie.setBackground(0,0,0)
    movie.setDimension(500,500)
    movie.setRate(12)
    movie.setFrames(1)
    stream = SWFVideoStream()
    stream.setDimension(500,500)
    item = movie.add(stream)
    item.setName('video')
    item.moveTo(0,0)
    strAction = open('/usr/share/univers/univers.as', 'r').read()
    movie.add(SWFAction(strAction))
    filename = '/usr/share/univers/univers-%s.swf' % __version__
    movie.save(filename)
    print "Built version %s of Univers Flash Video Player at: %s" % (__version__, filename)

if __name__ == '__main__':
    make()

