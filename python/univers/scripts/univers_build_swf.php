#!/usr/bin/php
<?php
if (!$argv[1]) {
   echo "Please specify version argument.\n"; 
   exit;
};
$version = $argv[1];
Ming_setScale(20.0000000);
ming_useswfversion(6);
$movie=new SWFMovie(6);
$movie->setBackground(0,0,0);
$movie->setDimension(500,500);
$movie->setRate(12);
$movie->setFrames(1);
$stream = new SWFVideoStream();
$stream->setDimension(500,500);
$item=$movie->add($stream);
$item->setname("video");
$item->moveto(0,0);
$strAction = file_get_contents('/usr/share/univers/univers.as');
$movie->add(new SWFAction($strAction));
$swfpath = "/usr/share/univers/univers-".$version.".swf";
$movie->save($swfpath,6);
echo "Built version $version of Univers Flash Video Player at: $swfpath\n";
exit;
?>
