LCK_FILENAME="queue_daemon.lck"
LCK_FILE=$1$LCK_FILENAME

if [ -f "${LCK_FILE}" ]; then
  PID=`head -n 1 "${LCK_FILE}"`
  TEST_RUNNING=`ps -p ${PID} | grep ${PID}`
  if [ -n "${TEST_RUNNING}" ]; then
    exit 0
  fi
fi

script="univers_daemon.sh "
$script$1 &> /dev/null &

exit 0
