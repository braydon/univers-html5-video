"""Installs UniversVideo using distutils"""

from distutils.core import setup
from distutils.command.install import INSTALL_SCHEMES
from distutils.command.install import install as _install
import sys
import os

REQUIRED_PYTHON_VERSION = '2.5'
INSTALLED_FILES = "installed_files"

class uninstall(_install):

    def run(self):
        try:
            file = open(INSTALLED_FILES, "r")
        except:
            self.warn("Could not read installed files list %s" % \
                          INSTALLED_FILES)
            return 
        files = file.readlines()
        file.close()
        files = [x.rstrip() for x in files]
        for file in files:
            print "Uninstalling %s" % file
            try:
                os.unlink(file)
            except:
                self.warn(sys.exc_info()[1])


class install(_install):

    def run(self):
        _install.run(self)
        outputs = self.get_outputs()
        data = "\n".join(outputs)
        try:
            file = open(INSTALLED_FILES, "w")
        except:
            self.warn("Could not write installed files list %s" % \
                          INSTALLED_FILES)
            return 
        file.write(data)
        file.close()


def main():

    #check platform
    if sys.platform.startswith("win") or sys.platform == "darwin":
        print "Sorry, this script currently doesn't work for"
        print "Windows or OS X." 
        sys.exit(-1)

    #check python version
    if sys.version < REQUIRED_PYTHON_VERSION:
        s = "I'm sorry, but UniversVideo requires Python %s or later. (Although this hasn't been checked.)"
        print s % REQUIRED_PYTHON_VERSION
        sys.exit(1)


    deps = {'ffmpeg':['/usr/local/bin/ffmpeg', '/usr/bin/ffmpeg'],
            'ffmpeg2theora': ['/usr/local/bin/ffmpeg2theora', '/usr/bin/ffmpeg2theora']}

    #check to see if ffmpeg and ffmpeg2theora are installed
    for application, paths in deps.items():
        installed = False
        for path in paths:
            if os.path.exists(path):
                installed = True
        if installed == False:
            print "I'm sorry, but UniversVideo requires %s to be installed." % application
            sys.exit(1)

    # set default location for "data_files" to
    # platform specific "site-packages" location
    for scheme in INSTALL_SCHEMES.values():
        scheme['data'] = scheme['purelib']

    ops = ("install", "build", "sdist", "uninstall", "clean")

    if len (sys.argv) < 2 or sys.argv[1] not in ops:
        print "Please specify operation : %s" % " | ".join (ops)
        raise SystemExit

    #do the installation
    dist = setup(
        name="UniversVideo",
        version="0.2",
        description="Video player / converter for HTML 5 Video and Flash",
        long_description="",
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Environment :: Web Environment",
            "Intended Audience :: Developers",
            "License :: OSI Approved :: GNU General Public License (GPL)",
            "Operating System :: POSIX :: Linux",
            "Programming Language :: Python, Unix Shell",
            "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
            "Topic :: Multimedia :: Graphics :: Presentation",
            "Topic :: Multimedia :: Graphics :: Viewers",
            "Topic :: Multimedia :: Video :: Display",
            "Topic :: Multimedia :: Video :: Conversion",
            ],
        author="Braydon Fuller",
        author_email="courier@braydon.com",
        url="http://video.braydon.com",
        license="GPL",
        packages=["univers","univers.scripts"],
        download_url="http://video.braydon.com/download/",
        data_files=[
            ('/usr/share/univers', ['univers/static/univers.as',
                                'univers/static/univers.js',
                                'univers/static/univers.swf',
                                'univers/static/share.png',
                                ]),
            ],
        scripts=["univers/scripts/univers_build_swf.py", 
                 "univers/scripts/univers_build_swf.php",
                 "univers/scripts/univers_convert.sh", 
                 "univers/scripts/univers_convert_simple.sh", 
                 "univers/scripts/univers_daemon.sh",
                 "univers/scripts/univers_start.sh",],
        cmdclass= {"uninstall" : uninstall,
                   "install" : install,},
    )


if __name__ == "__main__":
    main()
