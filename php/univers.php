<?php
  /*
   Plugin Name: Univers Video
   Plugin URI: http://video.braydon.com/
   Description: Provides ability to transcode/resize (using FFMPEG), and play videos with native support, using the HTML5 video tag and canvas element, in Firefox (Ogg Vorbis/Theora) and Safari (H264/AAC), and plugin support for Shockwave Flash (FLV), with the same (Firefox-like) controls. It intergrates with Wordpress Media Library so that when you upload a video, it automatically gets resized and transcoded to the configured formats. You can then insert the video into a post by browsing the library and clicking the "Insert into Post" button, which inserts a shortcode which will then be converted to the html needed to play the video. Note: This plugin requires that your server have ffmpeg, ffmpeg2theora, and the nessasary software packages to encode to the codecs ogg, mp3, aac, x264, and flv.
   Author: Braydon Fuller
   Version: 0.1
   Author URI: http://braydon.com/blog/
  */

define('FFMPEG_BINARY', '/usr/bin/ffmpeg');

class UniversVideo {

    function UniversVideo(){
        $plugindir_node = dirname(plugin_basename(__FILE__));	
        $this->PLUGINDIR_URL = get_bloginfo('wpurl') . "/wp-content/plugins/". $plugindir_node;
        $this->REPROCESS = true;
        $this->WIDTHS = array(480, 640);
        $this->VIDEO_BASE_PATH = ABSPATH . "wp-content/video/"; #xxx: update references to this with the width dir
        $this->VIDEO_BASE_URL = get_bloginfo('wpurl') . "/wp-content/video/"; #xxx: dito ^
        foreach ($this->WIDTHS as $w) {
            if(!file_exists($this->VIDEO_BASE_PATH . $w)){
                chdir(ABSPATH . "wp-content/video/");
                mkdir($w);
            }
        }
        $running_file = $this->VIDEO_BASE_PATH . 'running';
        if(!file_exists($running_file)){
            touch($running_file);
        }
        $queue_file = $this->VIDEO_BASE_PATH . 'queue';
        if(!file_exists($queue_file)){
            touch($queue_file);
        }
        add_filter('attachment_fields_to_edit', array(&$this, 'show_video_fields_to_edit'), 11, 2);
        add_filter('media_send_to_editor', array(&$this,'video_send_to_editor_shortcode'), 10, 3 );
        add_shortcode('universvideo', array(&$this, 'tag_replace'));
        add_action('delete_attachment', array(&$this, 'delete_attachment'));
        wp_enqueue_script('universvideo', plugins_url('/univers/player/univers.js'));
    }

    function delete_attachment($postid){
        $src = get_attached_file($postid);
        $info = pathinfo($src);
        $file_extensions = array('.ogg', '.mp4', '.jpg', '.ogg.log', '.mp4.log', '.jpg.log');
        foreach ($this->WIDTHS as $w) {
            $base = $this->VIDEO_BASE_PATH . $w . "/" . $info['filename'];
            foreach ($file_extensions as $ext) {
                $file_path = $base . $ext;
                if (file_exists($file_path)) {
                    unlink($file_path);
                }
            }    
        }
    }

    function is_video($post_id) {
        $src = get_attached_file($post_id);
        $srcp = pathinfo($src);
        $ext = strtolower($srcp['extension']);
        if ($ext == 'ogg' || $ext == 'mp4' || $ext == 'flv' ||
            $ext == 'avi' || $ext == 'wmv' || $ext == 'm4v' || $ext == 'mov' ||
            $ext == 'ogv' ||
            0 === strpos(get_post_mime_type($post_id), 'video/') ||
            0 === strpos(get_post_mime_type($post_id), 'application/octet-stream'))
            return true;
    }

    function numeral_to_alpha($num){
        $digits = array('zero','one','two','three','four','five','six','seven','eight','nine');
        foreach ($digits as $digit=>$alpha){
            $num = preg_replace("/".$digit."/", $alpha, $num);
        }
        return $num;
    }

    function video_embed($post, $width_index) {
        $media_meta = wp_get_attachment_metadata($post->ID);
	$src = get_attached_file($post->ID);
        if(!array_key_exists('height', $media_meta)){
            $video_info = $this->get_video_info($src);
	    $media_meta['height'] = $video_info['height'];
	    $media_meta['width'] = $video_info['width'];
	    $media_meta['duration'] = $video_info['duration'];
	    wp_update_attachment_metadata($post->ID,  $media_meta);
	}
        if(!$this->queued($post)){
            if(!$this->processing($post)){
                if($this->completed($post)){
                    $width = $this->WIDTHS[$width_index];
                    $height = $width / $media_meta['width'] * $media_meta['height'];
		    $heightplus = $height + 15;
                    $embed_width = $this->WIDTHS[0];
                    $embed_height = $embed_width / $media_meta['width'] * $media_meta['height'];
                    $srcp = pathinfo($src);
                    $flash_url = $this->PLUGINDIR_URL . "/player/univers.swf";
                    $id = $this->numeral_to_alpha($post->ID);
                    if ($width_index == 1){
                        $autoplay = "true";
                    } else {
                        $autoplay = "false";
                    }
                    $blog_title = get_bloginfo('name');
                    $site_url = get_bloginfo('siteurl');
                    if ($post->post_parent){
                        $link = get_permalink($post->post_parent);
                    } else {
                        $link = get_permalink($post->ID);
                    }
                    $poster = $this->VIDEO_BASE_URL . $width . "/" . $srcp['filename'] . ".jpg";
                    $mpfour = $this->VIDEO_BASE_URL . $width . "/" . $srcp['filename'] . ".mp4";
                    $ogg = $this->VIDEO_BASE_URL . $width . "/" . $srcp['filename'] . ".ogg";
		    $bgcolor = '000000';
		    $font_size = 13;
		    $font_family = 'Arial';
		    $object_url = $this->PLUGINDIR_URL . "/embed.php?id=" . $post->ID;

                    $div = '<div class="universvideo" id="video_'.$id.'" style="width:'.$width.'px'.';height:'.$height.'px;">
				<!-- "Video for Everybody" by Kroc Camen <camendesign.com> cc-by -->
				<video width="' . $width . '" height="' . $height . '" controls="controls" poster="' . $poster . '">
				    <source src="' . $ogg . '" type="video/ogg" />
				    <source src="' . $mpfour . '" type="video/mp4" />
				    <object width="' . $width . '" height="' . $height . '" type="application/x-shockwave-flash"
					    data="' . $flash_url . '" flashvars="autoplay=' . $autoplay . '&width=' . $width . '&height=' . $height . '&embed_width=' . $embed_width . '&embed_height=' . $embed_width . '&base_url=' . $this->VIDEO_BASE_URL . '&base_file=' . $srcp['filename'] . '&video_title=' . htmlspecialchars($post->post_title, ENT_QUOTES) . '&video_url= ' . $mpfour . '&site_title=' . $blog_title . '&site_url=' . $site_url . '&bgcolor=' . $bgcolor . '&object_url=' . $object_url . '&font_size=' . $font_size . '&font_family=' . $font_family . '&flash_url=' . $flash_url . '" wmode="transparent">
				    <param name="movie" value="' . $flash_url . '" />
				    <param name="wmode" value="transparent" /> 
				    <param name="flashvars" value="autoplay=' . $autoplay . '&width=' . $width . '&height=' . $height . '&embed_width=' . $embed_width . '&embed_height=' . $embed_width . '&base_url=' . $this->VIDEO_BASE_URL . '&base_file=' . $srcp['filename'] . '&video_title=' . htmlspecialchars($post->post_title, ENT_QUOTES) . '&video_url= ' . $mpfour . '&site_title=' . $blog_title . '&site_url=' . $site_url . '&bgcolor=' . $bgcolor . '&object_url=' . $object_url . '&font_size=' . $font_size . '&font_family=' . $font_family . '&flash_url=' . $flash_url . '" />
				    <!--[if gt IE 6]>
				    <object width="' . $width . '" height="' . $heightplus . '" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B">
				    <param name="autoplay" value="' . $autoplay . '" />
				    <param name="src" value="' . $mpfour . '" /><!
				    [endif]--><!--[if gt IE 6]><!-->

				    <object width="' . $width . '" height="' . $heightplus . '" type="video/quicktime" data="' . $mpfour . '">
				    <param name="autoplay" value="' . $autoplay . '" />
				    <param name="src" value="'. $mpfour .'" />
				    <!--<![endif]-->
				    <img src="' . $poster . '" alt="[' . $post->post_title . ']" />
				    <p>Download Video: <a href="' . $ogg . '">Ogg Theora format</a> |
				    <a href="' . $mpfour . '">MPEG-4 format</a> 
				    </p><!--[if gt IE 6]><!--></object><!--<![endif]-->

				    <!--[if gt IE 6]></object><![endif]-->
				    </object>
				</video>
				<!-- End "Video for Everybody" -->
                            </div>';

                    $script = "<script>video_".$id." = new Video({
                       'id':'video_".$id."',
                       'size':[". $width .",".$height."],
                       'embed_size':[".$embed_width.",".$embed_height."],
                       'base_url':'". $this->VIDEO_BASE_URL ."',
                       'base_file':'". $srcp['filename'] . "',
                       'flash':'".$flash_url."',
                       'autoplay':".$autoplay.",
                       'bgcolor':".$bgcolor.",
                       'display':{
                           'font_family':'".$font_family."',
                           'font_size':".$font_size.",
                           'site_title':'" . htmlspecialchars($blog_title) . "',
                           'site_url':'" . $site_url . "',
                           'video_title':'". htmlspecialchars($post->post_title, ENT_QUOTES) ."',
                           'video_url':'".$link."',
                           'object_url':'" . $object_url . "'
                        }});</script>";
                    $html = $div . $script;
                    if (!is_single()){
                        $html .= '<p class="universvideoinfo" style="width:'. $width .'px;"><a href="'.$link.'">View Larger</a></p>';
                    }
                    return $html;
                } else {
                    $msg = "There may have been an error in proccessing the video, some files are missing.";
                    if ($this->REPROCESS){
                        $this->process_video($post->ID);
                        $msg = "Adding the video to the process queue. Try viewing again later...";
                    }
                    return $msg;
                }
            } else {
                return "The video is currently processing. Please try viewing again later...";
            }
        } else {
            $this->queue_daemon();
            return "The video is currently in the queue to be processed. Please try viewing again later...";
        }
    }

    function tag_replace($attr){
        if (!$post = get_post($attr[0]))
            return "Video $attr[0] Not Found";
        $post = get_post($attr[0]);
        $parent_id = get_the_id();
        if ($post->post_parent != $parent_id){
            $mypost = array();
            $mypost['ID'] = $post->ID;
            $mypost['post_parent'] = $parent_id;
            wp_update_post($mypost);
        }
        if (is_single()){
            return $this->video_embed($post, 1, false);
        }
        return $this->video_embed($post, 0, false);
    }

    function video_send_to_editor_shortcode( $html, $post_id, $attachment ){
        if (!$this->is_video($post_id))  {
            return $html;
        }
        return "[universvideo $post_id]"; 
    }

    function process_video($post_id){
        if (!$post = get_post($post_id))
            return false; 
        if (!$this->is_video($post_id))
            return false;
        $src = get_attached_file($post->ID);
        $srcp = pathinfo($src);
        $media_meta = wp_get_attachment_metadata($post_id);
        $start_snapshot = $media_meta['duration']/2;
        $que_base = $this->VIDEO_BASE_PATH;
        foreach ($this->WIDTHS as $width){
            $height = $width / $media_meta['width'] * $media_meta['height'];
            if($height % 2 > 0){ $height = 2 * floor($height / 2);}
            $dst_base = $this->VIDEO_BASE_PATH . $width . "/" . $srcp['filename'];
            $cmd = ABSPATH . "wp-content/plugins/univers/scripts/queue_add.sh $src $width $height $start_snapshot $dst_base $que_base ". $media_meta['width'] ." ". $media_meta['height'];
            exec($cmd, $out);
        }
        $this->queue_daemon();
    }

    function get_video_info($src){
        $cmd = FFMPEG_BINARY . ' -i ' . $src  . ' 2>&1'; 
        $lines = array(); 
        exec($cmd, $lines); 
        $width = $height = 0; 
        foreach ($lines as $line) {
            if (preg_match('/Stream.*Video:.* (\d+)x(\d+).*/', $line, $matches)) {
                $width      = $matches[1]; 
                $height     = $matches[2]; 
            }
            if (preg_match('/Duration:\s*([\d:.]+),/', $line, $matches))
                $duration = $matches[1]; 
        }
        $n = preg_match('/(\d+):(\d+):(\d+)./', $duration, $match);         
        $total_seconds = 3600 * $match[1] + 60 * $match[2] + $match[3]; 
        return array('width' => $width, 'height' => $height, 'duration' => $total_seconds);
    }

    function completed($post){
        $src = get_attached_file($post->ID);
        $srcp = pathinfo($src);
        $completed = true;
        foreach ($this->WIDTHS as $w){
            $srcbase = $this->VIDEO_BASE_PATH . $w . "/" . $srcp['filename'];
            if (file_exists($srcbase . ".ogg") && file_exists($srcbase . ".jpg") && 
                file_exists($srcbase . ".mp4")){
            } else {
                $completed = false;
            }
        }
        return $completed;
    }

    function processing($post){
        $f = file($this->VIDEO_BASE_PATH . 'running');
        if ($f[0]){
            $src = get_attached_file($post->ID);
            $src = str_replace("/","\/",$src);
            if(preg_match('/'.$src.'/',$f[0]))
                return true;
        }
        return false;
    }

    function queued($post){
        $f = file($this->VIDEO_BASE_PATH . 'queue');
        $src = get_attached_file($post->ID);
        $src = str_replace("/","\/",$src);
        foreach ($f as $line_number => $line){
            if(preg_match('/'.$src.'/',$line))
                return true;
        }
        return false;
    }

    function queue_daemon(){
        $que_base = $this->VIDEO_BASE_PATH;
        $script_base = ABSPATH . "wp-content/plugins/univers/scripts/";
        $cmd = $script_base. "start_daemon.sh $que_base $script_base";
        exec($cmd, $out);
    }

    function show_video_fields_to_edit($fields, $post){
        if ( !$this->is_video( $post ) ) {
            return $fields;
        }
        unset($fields['url']);
        $post_id = $post->ID; 
        $embed_args = array( 'post_id' => $post_id, 'width' => 256, 'context' => 'admin' );
        $video_html = $this->video_embed($post, 0, true); 
        if ( 0 === strpos( $status, 'error_' ) ) {
            $video_html .= '<script type="text/javascript">jQuery(function($){$("[name=\'send['.$post_id.']\']").hide();});</script>';
        } else {
            $video_html = '<p>'.__('Shortcode for embedding:' ).' <strong>'.$this->video_send_to_editor_shortcode( '', $post_id, '' ).'</strong></p>'.$video_html;
        }
        $srcinfo = pathinfo(get_attached_file($post->ID));
        $log_files_contents = array();
        foreach ($this->WIDTHS as $w){
            $log_base = $this->VIDEO_BASE_PATH . $w . "/" . $srcinfo['filename'];
            $log_files = array('mp4' => $log_base . '.mp4.log', 'ogg' => $log_base . '.ogg.log', 'jpg' => $log_base . '.jpg.log');
            foreach ($log_files as $key => $file) {
                if(file_exists($file)){
                    $log_files_contents[$w ."-". $key] = file_get_contents($file);
                }
            }
        }
        $id = $this->numeral_to_alpha($post->ID);
        $log_html = "<p> <span id='showlog-". $id ."'>Show</span> | <span id='hidelog-". $id ."'>Hide</span> </div><div id='log-".$id."'";
        foreach ($log_files_contents as $key => $content){
            $log_html .= "<h2>".$key."</h2>";
            $log_html .= "<p>" . str_replace("\n", "<br/>", $content) . "</p>";
        }
        $log_html .=  "</div><script type='text/javascript'>
                                 jQuery('#log-".$id."').hide();
                                 jQuery('#showlog-".$id."').click(function(){jQuery('#log-".$id."').show();});
                                 jQuery('#showlog-".$id."').mouseover(function(){jQuery('#showlog-".$id."').css('color','#D54E21');});
                                 jQuery('#showlog-".$id."').mouseout(function(){jQuery('#showlog-".$id."').css('color','#333');});
                                 jQuery('#showlog-".$id."').css('cursor','pointer');
                                 jQuery('#hidelog-".$id."').click(function(){jQuery('#log-".$id."').hide();});
                                 jQuery('#hidelog-".$id."').mouseover(function(){jQuery('#hidelog-".$id."').css('color','#D54E21');});
                                 jQuery('#hidelog-".$id."').mouseout(function(){jQuery('#hidelog-".$id."').css('color','#333');});
                                 jQuery('#hidelog-".$id."').css('cursor','pointer');
                             </script>";

        $fields['video-preview'] = array(
                                         'label' => __( 'Preview and Insert' ),
                                         'input' => 'html',
                                         'html'  => $video_html,
                                         ); 
        $fields['video-status'] = array(
                                        'label' => __( 'Process Logs'),
                                        'input' => 'html',
                                        'html' => $log_html,
                                        );

        return $fields;
    }

}

$universvideo = new UniversVideo();

?>