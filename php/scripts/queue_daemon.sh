#!/bin/bash

# File        : Foxy Video Queue Processing Daemon
# Author      : Braydon Fuller
# URL         : http://www.braydon.com

#check to see if this daemon is already running, create lock file
LCK_FILENAME="queue_daemon.lck"
LCK_FILE=$1$LCK_FILENAME

if [ -f "${LCK_FILE}" ]; then
  MYPID=`head -n 1 "${LCK_FILE}"`
  TEST_RUNNING=`ps -p ${MYPID} | grep ${MYPID}`
  if [ -z "${TEST_RUNNING}" ]; then
    echo $$ > "${LCK_FILE}"
  else
    exit 0
  fi
else
  echo $$ > "${LCK_FILE}"
fi

#set the absolute path for the running file, $1 (arg1) for the base
running_filename='running'
running_file=$1$running_filename
queue_filename='queue'
queue_file=$1$queue_filename

#start a loop that will be repeated every 10 seconds
while [ 0 -eq 0 ] ; do
    let start=0
    #open the running file
    exec 6<$running_file
    read LINE <&6
    exec 6>&-
    #if the running file is empty, set start to true
    if [ ${#LINE} -eq 0 ] ; then
        let start=1
    else
        #other wise we will split the line to get the arguments
        IFS=" "
        set -- $LINE
        args=( $LINE )
        #the name of the file being processed
        FILE=${args[0]}
        let count=1
        declare -a OLDPIDS
        #the process ids that are working on the file
        while [ $count -lt ${#args} ] ; do
            OLDPIDS[$count-1]=${args[count]}
            ((count++))
        done
        let count=0
        let start=1
        #check to see if the pids are running
        while [ $count -lt ${#OLDPIDS} ] ; do
            kill -0 ${OLDPIDS[$count]} &> /dev/null 
            status=$?
            echo ${OLDPIDS[$count]}
            echo $status
            if [ $status -eq 0 ] ; then
                let start=0
            fi
            ((count++))
        done
    fi
    #if the process ids in the running file are not running, start the next process
    if [ $start -eq 1 ] ; then
        echo "" > $running_file
        exec 6<$queue_file
        read LINE <&6
        exec 6>&-
        sed -i 1d $queue_file
        
        #if the queue is empty shutdown, and cleanup the lock file
        if [ ${#LINE} -eq 0 ] ; then
            rm -f "${LCK_FILE}"
            exit 0
        fi

        IFS=" "
        set -- $LINE
        args=( $LINE )
        s=" "
        base=`dirname $0`
        script="/process_video_simple.sh"
        $base$script ${args[0]} ${args[1]} ${args[2]} ${args[3]} ${args[4]} ${args[5]} ${args[6]} &> /dev/null &
        pids=$!
        echo ${args[0]}$s$pids > $running_file
        pids=""
    fi
    #pause here for 10 seconds, will then repeat the process
    sleep 10
done

#incase the script ever get to this point, remove the lockfile and exit
rm -f "${LCK_FILE}"
exit 0