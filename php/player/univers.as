// Copyright (c) 2009, Braydon Fuller

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

aspect = _root.width / _root.height;
if (aspect > 1){
    cc = 500 / (_root.height); // compensate scale ratio
    dd =  (500 - (_root.width * cc)) / 2; // compensate shift
} else {
    cc = 500 / _root.width;
    dd =  (500 - ((_root.height) * cc)) / 2;
}
var w = _root.width * cc; // compensated width
var h = _root.height * cc; // compensated height
video._width = w; video._height = h;
if (aspect > 1){ tl = [dd, 0]; // top left corner: x,y
} else { tl = [0, dd];}
var tr = [tl[0]+(w), tl[1]]; // top right corner
var br = [tr[0], tr[1]+(h)]; // bottom right corner
var bl = [tl[0], tl[1]+(h)]; // bottom left corner
controller_x = (28*cc) + bl[0];
controller_y = bl[1] - (15*cc) ;
video._x = tl[0];
video._y = tl[1];
var nc=new NetConnection();
nc.connect(null);
var stream=new NetStream(nc); 
video.attachVideo(stream);
stream.setBufferTime(10); 

this.createEmptyMovieClip("flv_mc", 300);
flv_mc.attachAudio(stream);
var audio_sound = new Sound(flv_mc);
audio_sound.setVolume(100);
this.createEmptyMovieClip("poster_mc", 0);
with (poster_mc) {
    _alpha = 100;
    _x = tl[0];
    _y = tl[1];
    _xscale = cc * 100;
    _yscale = cc * 100;
};
poster_mc.loadMovie(_root.base_url + _root.width + "/" + _root.base_file + ".jpg");
getTime = function(){
    percent = stream.bytesLoaded / stream.bytesTotal * 100;
    time = 0;
    if (stream.time){time = stream.time;}
    display();
};
stream.onMetaData = function(obj){duration = obj.duration;};
var playing = true;
var muted = false;
var time_interval = setInterval(getTime,5);
if(_root.autoplay == 'false'){playing=false;} 
else {
poster_mc._alpha = 0;stream.play(_root.base_url + _root.width + "/" + _root.base_file + ".mp4");
stream.seek(_root.time);
}
var controls_visible = true;
init_mouse_events = function(){
    this.createEmptyMovieClip('hotspot', 1000);
    with (hotspot) {
	_alpha = 0;
	beginFill(0xffffff);
	moveTo(tl[0]+3,tl[1]+2);
	lineTo(tr[0]-2,tr[1]+2);
	lineTo(br[0]-2,br[1]-4);
	lineTo(bl[0]+3,bl[1]-4);
	endFill();
    }
    hotspot.onRollOver = function() {show_controls(true);};
    hotspot.onRollOut = function() {show_controls(false);};
    hotspot.onPress = function() {
        embedcodesOff(true);
	if (_root._xmouse >= (br[0] - 110*cc) && _root._ymouse >= (bl[1] - 28*cc)){
	} else if (_root._xmouse <= (bl[0] + 28*cc) && _root._ymouse >= (bl[1] - 28*cc)){
	} else if (_root._ymouse >= (bl[1] - 28*cc)){
	    seeking = true;
	}
    };

    hotspot.onMouseMove = function() {
	if (seeking) { 
	    stream.seek(((_root._xmouse - controller_x) / (duration_bar._width - (15*cc))) * duration);
	}
    };

    hotspot.onRelease = function() {
	if (_root._xmouse >= (br[0] - 83*cc) && _root._ymouse >= (bl[1] - 28*cc)){
            if (embedcodes._y == 0){
                embedcodes._y = -10000;
            } else {
                embedcodes._y = 0;
            };
	} else if (_root._xmouse >= (br[0] - 109*cc) && _root._xmouse <= (br[0] - 84*cc) && _root._ymouse >= (bl[1] - 28*cc)){
	    if( _root.audio_sound.getVolume() == 0){
		muted = false;
		_root.audio_sound.setVolume(100);
		volume_on._alpha = 100;
	    } else {
		muted = true;
		_root.audio_sound.setVolume(0);
		volume_on._alpha = 0;
	    }
	} else if (_root._xmouse <= (bl[0] + 28*cc) && _root._ymouse >= (bl[1] - 28*cc)){
	    if(playing == false){
		poster_mc._alpha = 0;
                if (stream.bytesLoaded){
                    stream.pause();
                } else {
                    stream.play(_root.base_url + _root.width + "/" + _root.base_file + ".mp4");
                }
		playing=true;
		pause_button._alpha = 100;
		play_button._alpha = 0;
		play_button.swapDepths(21);
		pause_button.swapDepths(22);
	    } else {
		stream.pause();playing=false;
		pause_button._alpha = 0;
		play_button._alpha = 100;
		pause_button.swapDepths(21);
		play_button.swapDepths(22);
	    }
	} else {
	    if (seeking) { 
		var t =  ((_root._xmouse - controller_x) / (duration_bar._width - (15*cc))) * duration;
		stream.seek(t);
	    }
	    seeking = false;
	}
    };
    hotspot.useHandCursor = false;
};

show_controls = function(boolean){
    if (boolean == false){
	controls_visible = false;
	pause_button._alpha = 0;
	play_button._alpha = 0;
	duration_bar._alpha = 0;
	progress_bar._alpha = 0;
	time_bar._alpha = 0;
	volume._alpha = 0;
	volume_on._alpha = 0;
	controller_bg._alpha = 0;
        embedvector._alpha = 0;
        embedvector.share._y = -1000000;
    } else {
        embedvector.share._y = -4*cc;
        embedvector._alpha = 90;
	controls_visible = true;
	if (playing == true){
	    pause_button._alpha = 100;
	    play_button._alpha = 0;
	} else {
	    pause_button._alpha = 0;
	    play_button._alpha = 100;
	}
	duration_bar._alpha = 80;
	progress_bar._alpha = 80;
	time_bar._alpha = 100;
	if (muted == false){
	    volume_on._alpha = 100;
	};
	volume._alpha = 100;
	controller_bg._alpha = 70;
    };
};
show_controls(false);
attr_tag = function(logo_align){
    return '<p class="universvideo" style="margin-top:5px;width:'+_root.embed_width+'px;font-family:'+_root.font_family+';font-size:'+_root.font_size+'px;"><a href="'+_root.video_url+'">'+_root.video_title+' (View Larger)</a></p>';
};
display_init = function() {
    this.createEmptyMovieClip('controller_bg', 20);
    with(controller_bg){
	_alpha = 0;
	beginFill(0x1f1f1f);
	moveTo(bl[0],bl[1]);
	lineTo(br[0],br[1]);
	lineTo(br[0],br[1]-(28*cc));
	lineTo(bl[0],bl[1]-(28*cc));
	endFill();
    };
    this.createEmptyMovieClip('pause_button', 21);
    with(pause_button) {
	_y = bl[1] - (21*cc);_x = (9*cc) + bl[0];_alpha=0;
	beginFill(0xffffff);moveTo(0,0);lineTo(0,12*cc);lineTo(4*cc,12*cc);lineTo(4*cc,0);endFill();
	beginFill(0xffffff);moveTo(7*cc,0);lineTo(7*cc,12*cc);lineTo(11*cc,12*cc);lineTo(11*cc,0);endFill();
    };
    this.createEmptyMovieClip('play_button', 22);
    with(play_button) {
	_y = bl[1] - (21*cc);_x = (9*cc) + bl[0];_alpha=0;
	beginFill(0xffffff);moveTo(0, 0);lineTo(9*cc, 6*cc);lineTo(0, 12*cc);endFill();
    };

    this.createEmptyMovieClip('duration_bar', 23);
    with (duration_bar) {
	_y = controller_y;
	_x = controller_x;
	_alpha = 0;
	lineStyle(8*cc, 0x9c9a9a, 80);
	moveTo(0,0);
	lineTo(w-(145*cc), 0);
    };
    this.createEmptyMovieClip('progress_bar', 24);
    with (progress_bar) {
	_y = controller_y;
	_x = controller_x;
	_alpha = 0;
	lineStyle(8*cc, 0xcac8c8, 80);
	moveTo(0,0);
	lineTo(7*cc,0);
    };
    this.createEmptyMovieClip('time_bar', 25);
    with (time_bar) {
	_y = controller_y - (9*cc);
	_x = controller_x;
	_alpha = 0;
	lineStyle(2.5*cc, 0xffffff, 100);
	beginFill(0x474747);
	moveTo(0,5*cc);
	curveTo(0,0,4.5*cc,0);
	curveTo(9*cc,0,9*cc,5*cc);
	lineTo(9*cc,13*cc);
	curveTo(9*cc,18*cc,4.5*cc,18*cc);
	curveTo(0,18*cc,0,13*cc);
	lineTo(0,5*cc);
	endFill();
    };

    this.createEmptyMovieClip('volume', 26);
    with (volume) {
	_y = controller_y - (7*cc);
	_x = br[0] - (101*cc);
	_alpha = 0;
	lineStyle(0, 0xffffff, 100);
	beginFill(0xffffff);
	moveTo(0, 5*cc);
	lineTo(0, 10*cc);
	lineTo(3*cc, 10*cc);
	lineTo(8*cc, 14*cc);
	lineTo(8*cc, 1*cc);
	lineTo(3*cc, 5*cc);
	endFill();
    };
    this.createEmptyMovieClip('volume_on', 27);
    with (volume_on){
	_y = controller_y - (3*cc);
	_x = br[0] - (89*cc);
	_alpha = 0;
	lineStyle(2.5*cc, 0xffffff, 100);
	moveTo(0,0);
	curveTo(5*cc,2.7*cc,-0.3*cc,7*cc);
    };

    this.createEmptyMovieClip('embedvector', 28);
    with (embedvector){
	_y = controller_y - (4*cc);
	_x = br[0] - (76*cc);
	_alpha = 0;
	lineStyle(2.5*cc, 0xffffff, 100);
	moveTo(5*cc, 0);
	lineTo(0, 4.5*cc);
	lineTo(5*cc, 9*cc);
	moveTo(12*cc, -2);
	lineTo(8*cc, 11*cc);
	moveTo(15*cc, 0);
	lineTo(20*cc, 4.5*cc);
	lineTo(15*cc, 9*cc);
    }

    shareFormat = new TextFormat();
    shareFormat.font = "Arial";
    shareFormat.size = 14 * cc;
    shareFormat.underline = false;
    shareFormat.color = 0xeeeeee;

    embedvector.createTextField('share', 180, 0,0,0,0);
    with (embedvector.share) {
        _x = 22*cc;
        _y = -1000000;
        autoSize = true;
        selectable = false;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = true;
        htmlText = "<b>SHARE</b>";
        setTextFormat(_root.shareFormat);
    }

    titleFormat = new TextFormat();
    titleFormat.font = _root.font_family;
    titleFormat.size = _root.font_size * cc;
    titleFormat.underline = false;
    titleFormat.color = 0x000000;

    titleundFormat = new TextFormat();
    titleundFormat.font = _root.font_family;
    titleundFormat.size = _root.font_size * cc;
    titleundFormat.underline = true;
    titleundFormat.color = 0x000000;

    this.createEmptyMovieClip('sitetitlemc', 29);
    sitetitlemc.createTextField('sitetitle', 128, 0,0,0,0);
    with (sitetitlemc.sitetitle) {
        _x = bl[0];
        _y = bl[1] + 5;
        autoSize = true;
        selectable = false;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = true;
        htmlText = _root.site_title;
        setTextFormat(_root.titleFormat);
    }
    sitetitlemc.onPress = function(){getURL(_root.site_url, "_blank");};
    sitetitlemc.onRollOver = function(){_root.sitetitlemc.sitetitle.setTextFormat(_root.titleundFormat);};
    sitetitlemc.onRollOut = function(){_root.sitetitlemc.sitetitle.setTextFormat(_root.titleFormat);};

    this.createTextField('spacer', 32, 0,0,0,0);

    with (spacer) {
        _x = bl[0] + sitetitlemc._width + 8;
        _y = bl[1] + 5;
        autoSize = true;
        selectable = false;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = true;
        htmlText = "/";
        setTextFormat(_root.titleFormat);
    }

    this.createEmptyMovieClip('titlemc', 30);
    titlemc.createTextField('title', 129, 0,0,0,0);
    with (titlemc.title) {
        _x = bl[0] + sitetitlemc._width + 28;
        _y = bl[1] + 5;
        autoSize = true;
        selectable = false;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = true;
        htmlText = _root.video_title;
        setTextFormat(_root.titleFormat);
    }

    titlemc.onPress = function(){getURL(_root.video_url, "_blank");};
    titlemc.onRollOver = function(){_root.titlemc.title.setTextFormat(_root.titleundFormat);};
    titlemc.onRollOut = function(){_root.titlemc.title.setTextFormat(_root.titleFormat);};


    showembedFormat = new TextFormat();
    showembedFormat.font = _root.font_family;
    showembedFormat.size = _root.font_size * cc;
    showembedFormat.underline = false;
    showembedFormat.color = 0x999999;

    this.createEmptyMovieClip('embedcodes', 1001);
    with (embedcodes) {
	_alpha = 90;
	beginFill(0x999999);
	moveTo(tl[0],tl[1]);
	lineTo(tr[0],tr[1]);
	lineTo(br[0],br[1]-(28*cc));
	lineTo(bl[0],bl[1]-(28*cc));
	endFill();
        _y = -1000;
    }

    embedFormat = new TextFormat();
    embedFormat.font = _root.font_family;
    embedFormat.size = _root.font_size * cc;
    embedFormat.underline = false;
    embedFormat.color = 0xffffff;

    embedcodeFormat = new TextFormat();
    embedcodeFormat.font = _root.font_family;
    embedcodeFormat.size = _root.font_size * 1.3 * cc;
    embedcodeFormat.underline = false;
    embedcodeFormat.color = 0x222222;

    embedcodes.createTextField('all', 130, 0,0,0,0);
    with (embedcodes.all) {
        autoSize = true;
        selectable = false;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = true;
        htmlText = "<b>HTML 5 Video or Flash</b> &nbsp; <i>(for your domain)</i>";
        setTextFormat(_root.embedFormat);
        _x = tl[0] + (w / 2) - (embedcodes.all._width / 2);
        _y = tl[1] + h * 0.13;
    }


    embedcodes.createTextField('flash', 131, 0,0,0,0);
    with (embedcodes.flash) {
        autoSize = true;
        selectable = false;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = true;
        htmlText = "<b>Flash</b>  &nbsp; <i>(for social networks)</i>";
        setTextFormat(_root.embedFormat);
        _x = tl[0] + (w / 2) - (embedcodes.flash._width / 2);
        _y = tl[1] + h * 0.35;
    }

    embedcodes.createTextField('url', 132, 0,0,0,0);
    with (embedcodes.url) {
        autoSize = true;
        selectable = false;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = true;
        htmlText = "<b>URL</b>  &nbsp; <i>(for email, microblogs, and chat)</i>";
        setTextFormat(_root.embedFormat);
        _x = tl[0] + (w / 2) - (embedcodes.url._width / 2);
        _y = tl[1] + h * 0.56;
    }

    embedurlcode = "http://";
    embedcodes.createTextField('urlcode', 133, 10,10,100,100);
    with (embedcodes.urlcode) {
        autoSize = false;
        _width = w*0.30;
        _height = _root.font_size * 3.2;
        border = 1;
        borderColor = 0x999999;
        background = true;
        backgroundColor = 0xeeeeee;
        selectable = true;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = false;
        text = _root.video_url;
        setTextFormat(_root.embedcodeFormat);
        _x = tl[0] + (w / 2) - (embedcodes.urlcode._width / 2);
        _y = tl[1] + h * 0.63;
    }

    var mlu = new Object();
    mlu.onMouseUp = function (){ 
        _global.setTimeout(Selection.setFocus, 5, embedcodes.urlcode);
    };
    mlu.onMouseDown = function (){  
        _global.setTimeout(Selection.setFocus, 5, embedcodes.urlcode);
    };
    embedcodes.urlcode.onSetFocus = function(){Mouse.addListener(mlu);};
    embedcodes.urlcode.onKillFocus = function(){Mouse.removeListener(mlu);};

    embedflashcode = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="' + _root.embed_width + '" height="' + _root.embed_height + '"><param name=movie value="' + _root.flash_url + '"><param name=quality value="high"><param name="wmode" value="transparent"><param name=FlashVars value="autoplay=false&width=' + _root.embed_width + '&height=' + _root.embed_height + '&embed_width='+_root.embed_width+'&embed_height='+_root.embed_height+'&base_url='+_root.base_url+'&base_file='+_root.base_file+'&video_title=' + _root.video_title + '&video_url=' + _root.video_url + '&site_title=' + _root.site_title + '&site_url=' + _root.site_url + '&bgcolor=' + _root.bgcolor + '&object_url=' + _root.object_url + '&font_size=' + _root.font_size + '&font_family=' + _root.font_family + '&flash_url=' + _root.flash_url + '"><param name=bgcolor value="' + _root.bgcolor + '"><embed src="' + _root.flash_url + '" FlashVars="autoplay=false&width=' + _root.embed_width + '&height=' + _root.embed_height + '&embed_width='+_root.embed_width+'&embed_height='+_root.embed_height+'&base_url='+_root.base_url+'&base_file='+_root.base_file+'&video_title=' + _root.video_title + '&video_url=' + _root.video_url + '&site_title=' + _root.site_title + '&site_url=' + _root.site_url + '&bgcolor=' + _root.bgcolor + '&object_url=' + _root.object_url + '&font_size=' + _root.font_size + '&font_family=' + _root.font_family + '&flash_url=' + _root.flash_url + '" quality="high" bgcolor="' + _root.bgcolor + '" width="' + _root.embed_width + '" height="'+ _root.embed_height +'" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent"></embed></object>' + attr_tag('left');
    embedcodes.createTextField('flashcode', 134, 0,0,0,0);
    with (embedcodes.flashcode) {
        autoSize = false;
        _width = w*0.80;
        _height = _root.font_size * 3.2;
        border = 1;
        borderColor = 0x999999;
        background = true;
        backgroundColor = 0xeeeeee;
        selectable = true;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = false;
        text = embedflashcode;
        setTextFormat(_root.embedcodeFormat);
        _x = tl[0] + (w / 2) - (embedcodes.flashcode._width / 2);
        _y = tl[1] + h * 0.42;
    }

    var mlf = new Object();
    mlf.onMouseUp = function (){ 
        _global.setTimeout(Selection.setFocus, 5, embedcodes.flashcode);
    };
    mlf.onMouseDown = function (){  
        _global.setTimeout(Selection.setFocus, 5, embedcodes.flashcode);
    };
    embedcodes.flashcode.onSetFocus = function(){Mouse.addListener(mlf);};
    embedcodes.flashcode.onKillFocus = function(){Mouse.removeListener(mlf);};
    embedallcode = '<object type="text/html" data="'+_root.object_url+'" style="width:'+_root.embed_width+'px;height:' + _root.embed_height + 'px;overflow:hidden;"></object>' + attr_tag('right');
    embedcodes.createTextField('allcode', 135, 0,0,0,0);
    with (embedcodes.allcode) {
        autoSize = false;
        _width = w*0.50;
        _height = _root.font_size * 3.2;
        border = 1;
        borderColor = 0x999999;
        background = true;
        backgroundColor = 0xeeeeee;
        selectable = true;
        embedFonts = false;
        multiline = false;
        wordWrap = false;
        html = false;
        text = embedallcode;
        setTextFormat(_root.embedcodeFormat);
        _x = tl[0] + (w / 2) - (embedcodes.allcode._width / 2);
        _y = tl[1] + h * 0.20;
    }

    var ml = new Object();
    ml.onMouseUp = function (){ 
        _global.setTimeout(Selection.setFocus, 5, embedcodes.allcode);
    };
    ml.onMouseDown = function (){  
        _global.setTimeout(Selection.setFocus, 5, embedcodes.allcode);
    };
    embedcodes.allcode.onSetFocus = function(){Mouse.addListener(ml);};
    embedcodes.allcode.onKillFocus = function(){Mouse.removeListener(ml);};

};
display_init();
display = function() {
    time_bar._x = ((duration_bar._width - (16*cc)) * stream.time / duration) + duration_bar._x;
    progress_bar.lineTo((duration_bar._width - (16*cc)) * percent / 100, 0);
};
init_mouse_events();